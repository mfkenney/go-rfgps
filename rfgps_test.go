package rfgps

import (
	"bytes"
	"strings"
	"testing"
	"time"
)

type rwbuffer struct {
	rbuf *bytes.Buffer
	wbuf *bytes.Buffer
}

func (b *rwbuffer) Read(p []byte) (int, error) {
	return b.rbuf.Read(p)
}

func (b *rwbuffer) Write(p []byte) (int, error) {
	return b.wbuf.Write(p)
}

func TestSample(t *testing.T) {
	resp := []byte("$001,091012,151400,47.123456,-122.567890,A,12.34,0.211,17.2\r\n")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	r := NewRfgps(&rw)
	r.SetDebug(true)

	rec, err := r.Poll(1)
	if err != nil {
		t.Fatalf("Read error: %v", err)
	}

	if rec.Address != 1 {
		t.Errorf("Bad address: %v", rec)
	}

	if rec.Status != "A" {
		t.Errorf("Bad status: %v", rec)
	}

	if rec.T.Year() != 2012 {
		t.Errorf("Bad timestamp: %v", rec)
	}
}

func TestError(t *testing.T) {
	resp := []byte("$001,-1,\"INVALID CMD\"\r\n")
	rw := rwbuffer{rbuf: bytes.NewBuffer(resp), wbuf: new(bytes.Buffer)}
	r := NewRfgps(&rw)
	_, err := r.Poll(1)
	_, ok := err.(*RfgpsError)
	if !ok {
		t.Errorf("Error not detected: %v", err)
	}
}

func TestCsv(t *testing.T) {
	input := strings.Split("1,1458259200,73.129602,-149.453145,A,12.46,1.471,-12.9", ",")
	tref, err := time.Parse(time.RFC3339, "2016-03-18T00:00:00Z")
	if err != nil {
		t.Fatal(err)
	}

	rec := &Record{}
	err = rec.UnmarshalCsv(input)
	if err != nil {
		t.Fatalf("Unmarshal error: %v", err)
	}

	if !tref.Equal(rec.T) {
		t.Fatalf("Timestamp mismatch: %q != %q", rec.T, tref)
	}

	output, _ := rec.MarshalCsv()
	for i, x := range output {
		if x != input[i] {
			t.Logf("Col[%d] mismatch: %v != %v", i, x, input[i])
		}
	}
}
