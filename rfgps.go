// The rfgps package provides an interface to the remote GPS units
// used on the ICEX Tracking Range.
package rfgps

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/mfkenney/go-nmea"
	"github.com/pkg/errors"
)

// RF-GPS data record
type Record struct {
	// Device address
	Address uint `json:"address"`
	// Record time-stamp
	T time.Time `json:"t"`
	// Latitude in degrees
	Lat float64 `json:"lat"`
	// Longitude in degrees
	Lon float64 `json:"lon"`
	// Fix status
	Status string `json:"status"`
	// Battery voltage
	Vbatt float32 `json:"vbatt"`
	// Battery current
	Ibatt float32 `json:"ibatt"`
	// Internal temperature
	Temp float32 `json:"temp"`
}

// Error returned from a unit
type RfgpsError struct {
	msg string
}

func (e *RfgpsError) Error() string {
	return e.msg
}

const time_format = "020106 150405"

// Communication interface
type Rfgps struct {
	rdr   *bufio.Reader
	wtr   io.Writer
	eol   string
	debug bool
}

func (r *Record) MarshalCsv() ([]string, error) {
	cols := make([]string, 8)
	cols[0] = strconv.FormatUint(uint64(r.Address), 10)
	cols[1] = strconv.FormatInt(r.T.Unix(), 10)
	cols[2] = strconv.FormatFloat(r.Lat, 'f', 6, 64)
	cols[3] = strconv.FormatFloat(r.Lon, 'f', 6, 64)
	cols[4] = r.Status
	cols[5] = strconv.FormatFloat(float64(r.Vbatt), 'f', 2, 32)
	cols[6] = strconv.FormatFloat(float64(r.Ibatt), 'f', 3, 32)
	cols[7] = strconv.FormatFloat(float64(r.Temp), 'f', 1, 32)

	return cols, nil
}

func (r *Record) ToCsv(w io.Writer, writeHeader bool) error {
	if writeHeader {
		fmt.Fprintln(w, "address,time,latitude,longitude,status,vbatt,ibatt,temp")
	}
	cols, _ := r.MarshalCsv()
	_, err := fmt.Fprintln(w, strings.Join(cols, ","))
	return err
}

func (r *Record) UnmarshalCsv(cols []string) error {
	var (
		ix  int64
		ux  uint64
		fx  float64
		err error
	)

	ux, err = strconv.ParseUint(cols[0], 10, 64)
	if err != nil {
		return err
	}
	r.Address = uint(ux)

	ix, err = strconv.ParseInt(cols[1], 10, 64)
	if err != nil {
		return err
	}
	r.T = time.Unix(ix, 0)

	r.Lat, err = strconv.ParseFloat(cols[2], 64)
	if err != nil {
		return err
	}

	r.Lon, err = strconv.ParseFloat(cols[3], 64)
	if err != nil {
		return err
	}

	r.Status = cols[4]

	fx, err = strconv.ParseFloat(cols[5], 32)
	if err != nil {
		return err
	}
	r.Vbatt = float32(fx)

	fx, err = strconv.ParseFloat(cols[6], 32)
	if err != nil {
		return err
	}
	r.Ibatt = float32(fx)

	fx, err = strconv.ParseFloat(cols[7], 32)
	if err != nil {
		return err
	}
	r.Temp = float32(fx)

	return nil
}

func conv_float(s string) float64 {
	f, _ := strconv.ParseFloat(s, 64)
	return f
}

func conv_uint(s string, base int) uint {
	u, _ := strconv.ParseUint(s, base, 32)
	return uint(u)
}

// Create a new RFGPS data record from a raw NMEA sentence
func ParseRecord(s *nmea.Sentence) (*Record, error) {
	if len(s.Fields) < 8 {
		return nil, errors.New("Invalid data message")
	}

	t, err := time.Parse(time_format, strings.Join(s.Fields[0:2], " "))
	if err != nil {
		return nil, errors.Wrap(err, "Invalid time stamp")
	}

	return &Record{
		Address: conv_uint(s.Id, 2),
		T:       t,
		Lat:     conv_float(s.Fields[2]),
		Lon:     conv_float(s.Fields[3]),
		Status:  s.Fields[4],
		Vbatt:   float32(conv_float(s.Fields[5])),
		Ibatt:   float32(conv_float(s.Fields[6])),
		Temp:    float32(conv_float(s.Fields[7])),
	}, nil
}

// NewRfgps returns a new Rfgps communication interface attached to an I/O
// port.
func NewRfgps(port io.ReadWriter) *Rfgps {
	r := Rfgps{eol: "\r\n"}

	r.rdr = bufio.NewReader(port)
	r.wtr = port

	return &r
}

// Enable or disable debugging output.
func (r *Rfgps) SetDebug(state bool) {
	r.debug = state
}

// Send sends a command to the device.
func (r *Rfgps) Send(addr uint, cmd string) error {
	text := fmt.Sprintf("$%s%03b%s", cmd, addr, r.eol)
	if r.debug {
		log.Printf("<%q\n", text)
	}

	_, err := r.wtr.Write([]byte(text))
	return err
}

// Read a response from a unit
func (r *Rfgps) Recv() ([]byte, error) {
	return r.rdr.ReadBytes('\n')
}

// Read a data record from the device. Return a pointer to the new record
// and any error that occurs.
func (r *Rfgps) Poll(addr uint) (*Record, error) {
	r.Send(addr, "S")
	raw, err := r.rdr.ReadBytes('\n')
	if err != nil {
		return nil, err
	}
	if r.debug {
		log.Printf(">%q", raw)
	}
	s, err := nmea.ParseSentence(raw)
	if err != nil {
		return nil, err
	}

	if len(s.Fields) < 2 {
		return nil, errors.New("Message too short")
	}

	switch s.Fields[0] {
	case "-1":
		return nil, &RfgpsError{msg: s.Fields[1]}
	case "PWR UP":
		// Power-on message
		return nil, nil
	}

	return ParseRecord(s)
}
